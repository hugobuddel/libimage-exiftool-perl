libimage-exiftool-perl (11.86-1) unstable; urgency=medium

  From upstream Changes for 11.86:

  - API Changes:
      - Patched ImageInfo() to recognize a stringified object as a file name

 -- gregor herrmann <gregoa@debian.org>  Sat, 08 Feb 2020 16:04:26 +0100

libimage-exiftool-perl (11.65-2) unstable; urgency=medium

  From upstream Changes for 11.71:

  - API Changes:
      - Changed ListSplit option to preserve empty list items

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Nov 2019 03:07:12 +0100

libimage-exiftool-perl (11.65-1) unstable; urgency=medium

  From upstream Changes for 11.64:

  - API Changes:
      - Documented SavePath and SaveFormat options

 -- gregor herrmann <gregoa@debian.org>  Thu, 29 Aug 2019 21:48:58 +0200

libimage-exiftool-perl (11.61-1) unstable; urgency=medium

  From upstream Changes for 11.58:

  - API Changes:
      - Removed PNGEarlyXMP option
      - Fixed problem introduced in 11.54 which caused Options('UserParam') to
        return undef

 -- gregor herrmann <gregoa@debian.org>  Fri, 09 Aug 2019 03:02:56 +0200

libimage-exiftool-perl (11.55-1) unstable; urgency=medium

  From upstream Changes for 11.54
  - API Changes:
      - Added FilterW option
      - Enhanced Compact option to improve flexibility and include features of
        XMPShorthand option
      - Removed XMPShorthand option from documentation

  From upstream Changes for 11.53
  - API Changes:
      - Enhanced XMPShorthand option to add level 2

  From upstream Changes for 11.52
  - API Changes:
      - Enhanced Compact option to add levels 3, 4 and 5

  From upstream Changes for 11.45
  - API Changes:
      - Added QuickTimeHandler option

  From upstream Changes for 11.34
  - API Changes:
      - Changed SetFileName() 'Link' option name to 'HardLink' (but still allow
        'Link' for backward compatibility)

  From upstream Changes for 11.33
  - API Changes:
      - Added SymLink option to SetFileName()

  From upstream Changes for 11.32
  - API Changes:
      - Added new single-argument version of ShiftTime()

  From upstream Changes for 11.19
  - API Changes:
      - Enhanced FastScan option to add a setting of 4

 -- gregor herrmann <gregoa@debian.org>  Sun, 14 Jul 2019 17:50:36 -0300

libimage-exiftool-perl (11.16-1) unstable; urgency=medium

  From upstream Changes for 11.15:
  - API Changes:
      - Added GeoSpeedRef option

 -- gregor herrmann <gregoa@debian.org>  Wed, 31 Oct 2018 18:50:40 +0100

libimage-exiftool-perl (11.13-1) unstable; urgency=medium

  From upstream Changes for 11.13:
  - API Changes:
      - Enhanced StrictDate option to reformat PNG CreateTime according to PNG
        specification

 -- gregor herrmann <gregoa@debian.org>  Wed, 10 Oct 2018 19:37:01 +0200

libimage-exiftool-perl (10.79-1) unstable; urgency=medium

  From upstream Changes for 10.76:
  - API Changes:
      - Added XMPShorthand option
      - Enhanced Compact option so a setting of 2 avoids XMP indentation

 -- gregor herrmann <gregoa@debian.org>  Wed, 14 Feb 2018 20:42:25 +0100

libimage-exiftool-perl (10.67-1) unstable; urgency=medium

  From upstream Changes for 10.67:
  - API Changes:
      - Added TimeZone option

 -- gregor herrmann <gregoa@debian.org>  Tue, 21 Nov 2017 20:25:13 +0100

libimage-exiftool-perl (10.56-1) unstable; urgency=medium

  From upstream Changes for 10.41, 10.45, 10.46, 10.49, 10.54:
  - API Changes:
      - Added ListJoin option to replace List and ListSep options
      - Allow access to the advanced formatting expression via a new ExifTool
        "FMT_EXPR" member variable
      - Enhanced RequestTags option to allow groups to be requested
      - Added XAttrTags option
      - Enhanced RequestAll option
      - Added experimental Validate option

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Jun 2017 21:16:05 +0200

libimage-exiftool-perl (10.23-1) unstable; urgency=medium

  From upstream Changes for 10.23:
  - API Changes:
      - Added CharsetRIFF option

 -- gregor herrmann <gregoa@debian.org>  Wed, 20 Jul 2016 20:20:28 +0200

libimage-exiftool-perl (10.14-1) unstable; urgency=medium

  From upstream Changes for 10.14:
  - API Changes:
      - Fixed bug where FileModifyDate wasn't set properly when WriteInfo() was
        called without a destination file name and other "real" tags were
        written at the same time

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 Apr 2016 01:56:43 +0200

libimage-exiftool-perl (10.13-1) unstable; urgency=medium

  From upstream Changes for 10.13:
  - API Changes:
      - Fixed bug which could cause the API Filter option to be ignored for
        some tags when copying tags with the Composite option set

 -- gregor herrmann <gregoa@debian.org>  Sun, 27 Mar 2016 20:47:31 +0200

libimage-exiftool-perl (10.10-1) unstable; urgency=medium

  From upstream Changes for 10.10:
  - API Changes:
      - Added RequestTags option
      - No longer generate MDItem tags when RequestAll option is set

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Jan 2016 00:29:20 +0100

libimage-exiftool-perl (10.08-1) unstable; urgency=medium

  From upstream Changes for 10.08:
  - API Changes:
      - Changed QuickTimeUTC API option to also enforce proper time zero

 -- gregor herrmann <gregoa@debian.org>  Wed, 23 Dec 2015 18:14:53 +0100

libimage-exiftool-perl (10.05-1) unstable; urgency=medium

  From upstream Changes for 10.05:
  - API Changes:
      - Added Filter option
      - Changed GetNewValues method name to GetNewValue (GetNewValues still
        works for backward compatibility)

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Nov 2015 18:10:55 +0100

libimage-exiftool-perl (9.27-1) unstable; urgency=low

  From upstream changes for the versions between 9.13 and 9.27:
  - API Changes:
   - The CombineInfo() routine is now deprecated because it is likely that
     nobody ever used it. If anyone actually uses this, please let me know
   - Compatibility Notice: The MWG Composite tags are no longer automatically
     loaded just by using the MWG module. Image::ExifTool::MWG::Load() must
     now be called explicitly to load these tags

 -- gregor herrmann <gregoa@debian.org>  Tue, 22 Oct 2013 19:44:34 +0200
